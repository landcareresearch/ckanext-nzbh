# NZBH CKAN Extension.

This repository creates the development environment for the nzbh ckan extension.

## Initialize Environment

The first step is to checkout sub-modules.  Run the following bash command:

```bash
./setup_dev_env.bash
```

Change directory to vagrant-control and run the following command.

```bash
./install.bash
```

### vagrant-control

This directory (git submodule) contains a vagrant project used for setting up a development virtual machine.

#### root_dir/credentials.rb

In order to allow the vagrant instance to access private bitbucket accounts necessary for the installation,
 you will need to create a new file that contains your bitbucket credentials.  Note, this file should not be committed to the repo as its your personal details.

Setup a credentials file that contains the path to your ssh keys that are used to authenticate with Bitbucket.

```bash
module MyCredentials
    SSH_KEY_PATH = "~/.ssh"
end
```

#### SSH_KEY_PATH

Most users will not need to change this as it should point to the default location of your ssh keys.

#### Modify root_dir/config.rb

The following variables should be changed to match the path to these repositories if not installed to the root_dir.

```bash
        "ckanext_nzbh" => {
            "src"  => "../../ckanext_nzbh",
            "dest" => "/opt/ckanext_nzbh"
        }
```

## Provisioning

Change directory to vagrant-control

```bash
cd vagrant-control
```

Run vagrant

```bash
./run.bash
```

Wait for vagrant to finish deploying and installing silverstripe
Wait for the console to return.  You will see the following line:

```bash
notice: Finished catalog run in 648.91 seconds

```

## Re-Provisioning

In the event there are administrative updates that need to be applied to the development environment, this section describes how to apply those changes.
In most cases, use the following command to apply admin changes.

```bash
./provision.bash
```

## Running

ssh access

```bash
vagrant ssh
```

## Coding

You can point your IDE at the docroot directory.  This should enable the possibility of properly debugging.  Changes that you make within the docroot will be reflected back to the various project directories like code and configure.  Just note, that any additions made to modules, cms, etc will not be captured!

## Builds

This repository can build and deploy to two test vm instances.  The build system uses submodules for testing.  In order to get the latest tests, please use the following command to push to the build branch.

```scripts/push.bash```

Before issuing the above command, make sure you have changed to the intended build branch.

### Details

* **Git Branch**: build
* **Teamcity Project**: [nzbh](http://build.landcareresearch.co.nz/project.html?projectId=LinuxCI_nzbh_ckanext&tab=projectOverview)
* **Deployment Server**: test-ckan-nzbh
* **Deployment End Point**: http://test-ckan-nzbh.landcareresearch.co.nz