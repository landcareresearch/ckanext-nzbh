module MyVars
    HOSTNAME               = "dev-ckan-nzbh"
    OS_FILE                = "ubuntu16.04"
    MEMORY                 = "2048"
    CPUS                   = "1"
    IP                     = "192.168.33.22"
    GENCACHE               = false
    PROJECT_PATH           = "../"
    FOLDERS                = {
        "ckanext-nzbh" => {
            "src"  => "../.",
            "dest" => "/opt/ckanext-nzbh"
        }
    }
end
